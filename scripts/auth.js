// Add admin cloud function
const adminForm = document.querySelector('.admin-actions');
adminForm.addEventListener('submit', (e) => {
	e.preventDefault();

	const adminEmail = document.querySelector('#admin-email').value;
	const addAdminRole = functions.httpsCallable('addAdminRole');
	addAdminRole({ email: adminEmail }).then((result) => {
		console.log(result);
	});
});

// listen for auth status changes
auth.onAuthStateChanged((user) => {
	if (user) {
		user.getIdTokenResult().then((idTokenResult) => {
			user.admin = idTokenResult.claims.admin;
			setupUi(user);
		});
		db.collection('guides').onSnapshot(
			(snapshot) => {
				setupGuides(snapshot.docs);
				setupUi(user);
			},
			(error) => {
				console.log(error);
			}
		);
	} else {
		setupUi();
		setupGuides([]);
	}
});

// create new guide
const createForm = document.querySelector('#create-form');
createForm.addEventListener('submit', (e) => {
	e.preventDefault();

	// add entry
	db.collection('guides')
		.add({
			title: createForm['title'].value,
			content: createForm['content'].value
		})
		.then(() => {
			// close modal and reset form
			const modal = document.querySelector('#modal-create');
			M.Modal.getInstance(modal).close();
			createForm.reset();
		})
		.catch((error) => {
			console.log(error.message);
		});
});

// signup
const signupForm = document.querySelector('#signup-form');
signupForm.addEventListener('submit', (e) => {
	e.preventDefault();

	// get user info
	const email = signupForm['signup-email'].value;
	const password = signupForm['signup-password'].value;

	// sign up the user
	auth.createUserWithEmailAndPassword(email, password)
		.then((credential) => {
			return db
				.collection('users')
				.doc(credential.user.uid)
				.set({ bio: signupForm['signup-bio'].value });
		})
		.then(() => {
			// close signup modal and reset form
			const modal = document.querySelector('#modal-signup');
			M.Modal.getInstance(modal).close();
			signupForm.reset();
			signupForm.querySelector('.error').innerHTML = '';
		})
		.catch((error) => {
			signupForm.querySelector('.error').innerHTML = error.message;
		});
});

// logout
const logout = document.querySelector('#logout');
logout.addEventListener('click', (e) => {
	e.preventDefault();

	// sign user out
	auth.signOut();
});

// login
const loginForm = document.querySelector('#login-form');
loginForm.addEventListener('submit', (e) => {
	e.preventDefault();

	// get user info
	const email = document.querySelector('#login-email').value;
	const password = document.querySelector('#login-password').value;

	// log in the user
	auth.signInWithEmailAndPassword(email, password)
		.then((credential) => {
			// close login modal and reset form
			const modal = document.querySelector('#modal-login');
			M.Modal.getInstance(modal).close();
			loginForm.reset();
			loginForm.querySelector('.error').innerHTML = '';
		})
		.catch((error) => {
			loginForm.querySelector('.error').innerHTML = error.message;
		});
});

// Your web app's Firebase configuration
var firebaseConfig = {
	apiKey: 'AIzaSyApaFAm8spM3KlrTm4NqlTpOggKlNfMZDA',
	authDomain: 'tobiaswaelde-firebase-auth.firebaseapp.com',
	databaseURL: 'https://tobiaswaelde-firebase-auth.firebaseio.com',
	projectId: 'tobiaswaelde-firebase-auth',
	storageBucket: 'tobiaswaelde-firebase-auth.appspot.com',
	messagingSenderId: '952946818642',
	appId: '1:952946818642:web:a68eab29e0916cc1'
};
// Initialize Firebase
firebase.initializeApp(firebaseConfig);

// make auth and firestore references
const auth = firebase.auth();
const db = firebase.firestore();
const functions = firebase.functions();

// update firestore settings
//db.settings({ timestampsInSnapshots: true });

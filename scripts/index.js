const guideList = document.querySelector('.guides');
const loggedOutLinks = document.querySelectorAll('.logged-out');
const loggedInLinks = document.querySelectorAll('.logged-in');
const accountDetails = document.querySelector('.account-details');
const adminItems = document.querySelectorAll('.admin');

// setup ui
const setupUi = (user) => {
	if (user) {
		if (user.admin) {
			adminItems.forEach((item) => (item.style.display = 'block'));
		}

		// toggle ui elements
		loggedInLinks.forEach((item) => (item.style.display = 'block'));
		loggedOutLinks.forEach((item) => (item.style.display = 'none'));

		// account info
		db.collection('users')
			.doc(user.uid)
			.get()
			.then((doc) => {
				const html = `
                    <div>Logged in as ${user.email}</div>
                    <div>${doc.data().bio}</div>
                    <div class="pink-text">${user.admin ? 'Admin' : 'Default User'}</div>
                `;
				accountDetails.innerHTML = html;
			});
	} else {
		// toggle ui elements
		loggedInLinks.forEach((item) => (item.style.display = 'none'));
		loggedOutLinks.forEach((item) => (item.style.display = 'block'));

		// hide account info
		accountDetails.innerHTML = '';

		adminItems.forEach((item) => (item.style.display = 'none'));
	}
};

// setup guides
const setupGuides = (data) => {
	if (data.length) {
		let html = '';
		data.forEach((doc) => {
			const guide = doc.data();
			const item = `
            <li>
                <div class="collapsible-header grey lighten-4">${guide.title}</div>
                <div class="collapsible-body">${guide.content}</div>
            </li>
        `;
			html += item;
		});
		guideList.innerHTML = html;
	} else {
		guideList.innerHTML = '<h5 class="center">Login to view guides</h5>';
	}
};

// setup materialize components
document.addEventListener('DOMContentLoaded', function() {
	const modals = document.querySelectorAll('.modal');
	M.Modal.init(modals);

	const items = document.querySelectorAll('.collapsible');
	M.Collapsible.init(items);
});
